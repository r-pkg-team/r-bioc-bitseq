Source: r-bioc-bitseq
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-bioc-rsamtools,
               r-bioc-s4vectors,
               r-bioc-iranges,
               r-bioc-rhtslib,
               libcurl4-openssl-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-bitseq
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-bitseq.git
Homepage: https://bioconductor.org/packages/BitSeq/
Rules-Requires-Root: no

Package: r-bioc-bitseq
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: transcript expression inference and analysis for RNA-seq data
 The BitSeq package is targeted for transcript expression
 analysis and differential expression analysis of RNA-seq data
 in two stage process. In the first stage it uses Bayesian
 inference methodology to infer expression of individual
 transcripts from individual RNA-seq experiments. The second
 stage of BitSeq embraces the differential expression analysis
 of transcript expression. Providing expression estimates from
 replicates of multiple conditions, Log-Normal model of the
 estimates is used for inferring the condition mean transcript
 expression and ranking the transcripts based on the likelihood
 of differential expression.
